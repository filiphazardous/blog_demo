# Blog demo

The purpose of this demo is to showcase a very simple CRUD back-end written in node. It comes with an api definition, a simple front-end, a simple back-end, and some tests.

## TLDR

Check the database section below to install mysql and create a database, then run:  
`npm i && MYSQL_PASSWORD=some_password npm run init` (once)  
`MYSQL_PASSWORD=some_password npm start` (whenever you want to develop)

## API

The API is documented in swagger format, and is available in the `api-doc` folder. For an interactive swagger editor - do check out https://editor.swagger.io

When running in dev-mode, the swagger can be viewed on http://localhost:3000/docs

## Front-end

Is built with `create react app`, I trust you know how it works. It's very bare-bones

## Back-end

For all development purposes, we define the back-end to run on http://localhost:3001

### Development

To run the back-end in development mode, cd into `back-end` and type `npm run dev`  
`babel-watch` will restart the server every time you save files.

### Production

To run the back-end in production mode, cd into `back-end` and type `npm run build` followed by `npm start`

### Database

You need to:
* Install mysql (if you don't already have it): `brew install mysql`
* Start mysql server: `mysql.server start`
* Login to mysql: `mysql -uroot` 
* Create a database (name it "blog_demo"): `CREATE DATABASE blog_demo;`
* Create a database user (name it "blog_demo"): `CREATE USER "blog_demo"@"localhost" IDENTIFIED BY "some_password";`
* Grant all privileges for the database to the user: `GRANT ALL ON blog_demo.* TO "blog_demo"@"localhost";`
* Update privilege tables: `FLUSH PRIVILEGES;`
* Exit mysql: `quit`

If you wish to call the database and/or user something other than "blog_demo", you can do that and setup the data in an environment variable named MYSQL_CONNECTION_STRING. However, this will break `npm init` - so you have to seed the database manually.  

If you follow the presumed naming convention, all you need to provide is the MYSQL_PASSWORD (see TLDR above)
