import loginController from '../controller/login';
import { Router } from 'express';

export default function authenticationRoutes (basePath, app = new Router(), logger = null, db = null) {
  app.post(`/${basePath}/login`, loginController(db, logger));
}
