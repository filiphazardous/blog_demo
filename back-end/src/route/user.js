import userController from '../controller/user';
import authentication from '../middleware/authentication';
import { Router } from 'express';

export default function userRoutes (basePath, app = new Router(), logger = null, db = null) {
  const route = `/${basePath}/user`;

  app.post(route, userController.postUser(db, logger));

  const routeWithUsername = `${route}/:username`;
  app.get(routeWithUsername, authentication(db, logger, true), userController.getUser(db, logger));
  app.put(routeWithUsername, authentication(db, logger, true), userController.putUser(db, logger));
  app.delete(routeWithUsername, authentication(db, logger, true), userController.deleteUser(db, logger));

  app.get(`/${basePath}/users`, authentication(db, logger, true), userController.getUsers(db, logger));
}
