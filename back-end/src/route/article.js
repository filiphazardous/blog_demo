import { Router } from 'express';
import articleController from '../controller/article';
import authentication from '../middleware/authentication';

export default function articleRoutes (basePath, app = new Router(), logger, db) {
  const route = `/${basePath}/article`;

  app.post(route, authentication(db, logger, true), articleController.postArticle(db, logger));

  const routeWithArticleId = `${route}/:articleId`;
  app.get(routeWithArticleId, authentication(db, logger, false), articleController.getArticle(db, logger));
  app.put(routeWithArticleId, authentication(db, logger, true), articleController.putArticle(db, logger));
  app.delete(routeWithArticleId, authentication(db, logger, true), articleController.deleteArticle(db, logger));

  app.get(`/${basePath}/articles`, authentication(db, logger, false), articleController.getArticles(db, logger));
}

