import { Router } from 'express';
import healthController from '../controller/health';

export default function (basePath, app = new Router(), logger, db) {
  app.get(`/${basePath}/healthcheck`, healthController(logger, db));
}
