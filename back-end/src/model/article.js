export default class Article {
  static filterKeys = ['createdAt', 'updatedAt', 'id', 'author', 'title', 'text', 'articleStatus', 'category'];
  static filterObject = (inputObject) => this.filterKeys.reduce(function (acc, key) {
    acc[key] = inputObject[key];
    return acc;
  }, {});

  constructor (db, logger) {
    this._db = db;
    this._logger = logger;
  }

  async add (article) {
    await this._db('article').insert(article);
    return this.fetchLatestByTitle(article.title).then(Article.filterObject);
  }

  basicFetch () {
    return this._db({ a: 'article' })
      .innerJoin({ u: 'user' }, 'a.author', 'u.id')
      .select('a.createdAt',
        'a.updatedAt',
        'a.id',
        'a.title',
        'a.text',
        'a.category',
        'a.articleStatus', {
          author: 'u.username',
        },
      );
  }

  fetchLatestByTitle (title) {
    return this.basicFetch()
      .where({ title })
      .orderBy('createdAt', 'desc')
      .first();
  }

  fetchById (id) {
    return this.basicFetch()
      .where({ 'a.id': id })
      .first();
  }

  async updateById (id, article) {
    await this._db('article').update(article).where({ id });
    return this.fetchById(id).then(Article.filterObject);
  }

  async deleteById (id) {
    await this._db('article').delete().where({ id });
  }

  async fetchByFilter ({ statusAr = null, username = null }) {
    const query = this.basicFetch();
    const queryWithStatusFilter = statusAr ? query.whereIn('articleStatus', statusAr) : query;
    const queryWithUsernameFilter = username ? queryWithStatusFilter.orWhere('u.username', username) : queryWithStatusFilter;

    // TODO: Add category to query - but what do we want the logic to look like?

    const finalQuery = queryWithUsernameFilter.orderBy('createdAt', 'desc');
    const result = await finalQuery;
    return result.map(Article.filterObject);
  }
}
