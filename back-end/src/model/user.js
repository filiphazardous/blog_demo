import bcrypt from 'bcryptjs';

export default class User {
  static filterKeys = ['createdAt', 'updatedAt', 'id', 'username', 'firstName', 'lastName', 'email', 'phone', 'userStatus'];
  static filterObject = (inputObject) => this.filterKeys.reduce(function (acc, key) {
    acc[key] = inputObject[key];
    return acc;
  }, {});

  constructor (db, logger) {
    this._db = db;
    this._logger = logger;
  }

  async add (user) {
    await this._db('user').insert(user);
    return this.basicFetchByUsername(user.username).then(User.filterObject);
  }

  async verifyLogin (username, incomingPassword) {
    const userObj = await this.basicFetchByUsername(username);
    if (!userObj) {
      throw new Error('User not registered');
    }
    const { isMatch } = await User.comparePassword(incomingPassword, userObj.password);

    return isMatch ? User.filterObject(userObj) : null;
  }

  basicFetchByUsername (username) {
    return this._db('user').where({ username }).first();
  }

  fetchByUsername (username) {
    return this.basicFetchByUsername(username).then(User.filterObject);
  }

  async fetchByStatus (statusAr = null) {
    const query = this._db('user');
    const queryWithFilter = statusAr ? query.whereIn('userStatus', statusAr) : query;
    const result = await queryWithFilter.orderBy('createdAt', 'desc');
    return result.map(User.filterObject);
  }

  async updateByUsername (username, user) {
    await this._db('user').update(user).where({ username });
    return this.basicFetchByUsername(user.username).then(User.filterObject);
  }

  async deleteByUsername (username) {
    await this._db('user').delete().where({ username });
  }

  static hashPassword (password) {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
  }

  static comparePassword (password, hashedPassword) {
    return new Promise(function (resolve, reject) {
      bcrypt.compare(password, hashedPassword, function (error, isMatch) {
        if (error) {
          console.error(error);
          return reject(error);
        }

        return resolve({ isMatch });
      });
    });
  }
}
