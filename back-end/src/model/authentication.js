import jwt from 'jsonwebtoken';
import UserModel from './user';
import { secret } from '../../jwtSecret.json';

const expiresIn = 600; // Set to 10 minutes, to trigger new logins during development

export default class Authentication {
  constructor (db, logger) {
    this._logger = logger;
    this._userModel = new UserModel(db, logger);
  }

  generate = ({ username, userStatus }) => jwt.sign({ username, userStatus }, secret, { expiresIn });

  authenticate (token) {
    const { username } = jwt.verify(token, secret);
    this._logger.log(`${username} authenticated via token`);
    return this._userModel.fetchByUsername(username);
  }
}
