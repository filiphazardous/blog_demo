import getUser from './user/getUser';
import putUser from './user/putUser';
import postUser from './user/postUser';
import deleteUser from './user/deleteUser';
import getUsers from './user/getUsers';

export default {
  getUser,
  putUser,
  postUser,
  deleteUser,
  getUsers,
};
