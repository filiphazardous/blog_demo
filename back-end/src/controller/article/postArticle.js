import Article from '../../model/article';

export default function postArticle (db, logger) {
  const articleModel = new Article(db, logger);

  return async function (request, response) {
    try {
      const {
        user
      } = request;
      logger.log(user);
      if (!['admin', 'active'].includes(user.userStatus)) {
        return response.sendStatus(403);
      }

      const {
        title,
        text,
      } = request.body;

      const article = { title, text, author: user.id };
      const result = await articleModel.add(article);

      return response.status(201).json(result);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
