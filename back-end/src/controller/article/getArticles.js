import Article from '../../model/article';

export default function getArticles (db, logger) {
  const articleModel = new Article(db, logger);

  return async function (request, response) {
    try {
      const {
        user,
        query: {
          status
        }
      } = request;

      const statusArQuery = typeof status === 'string' ? status.split(',') : null;
      const statusAr = user && user.userStatus === 'admin' ? statusArQuery : ['published'];
      const username = user && user.userStatus !== 'admin' ? user.username : null;

      const result = await articleModel.fetchByFilter({ statusAr, username });

      return response.status(200).json(result);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
