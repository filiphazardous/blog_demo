import Article from '../../model/article';

export default function putArticle (db, logger) {
  const articleModel = new Article(db, logger);

  return async function (request, response) {
    try {
      const {
        articleId,
      } = request.params;

      const {
        user,
      } = request;

      if (!['active', 'admin'].includes(user.userStatus)) {
        logger.error('Bad user status', user);
        return response.sendStatus(403);
      }

      const currentArticle = await articleModel.fetchById(articleId);
      if (!currentArticle) {
        return response.sendStatus(404);
      }
      if (currentArticle.author !== user.username && user.userStatus !== 'admin') {
        logger.error('User not author, and not admin either', currentArticle, user);
        return response.sendStatus(403);
      }

      const {
        title = currentArticle.title,
        text = currentArticle.text,
        articleStatus = currentArticle.articleStatus,
      } = request.body;

      const article = { title, text, articleStatus };

      const result = await articleModel.updateById(articleId, article);
      return response.status(200).json(result);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
