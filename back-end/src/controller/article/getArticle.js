import Article from '../../model/article';

export default function getArticle (db, logger) {
  const articleModel = new Article(db, logger);

  return async function (request, response) {
    try {
      const {
        user,
        params: {
          articleId,
        }
      } = request;

      const article = await articleModel.fetchById(articleId);
      if (!article || (article.articleStatus !== 'published' && !user)) {
        return response.sendStatus(404);
      }

      if (article.articleStatus === 'published' || article.author === user.username || user.userStatus === 'admin') {
        return response.status(200).json(article);
      }

      return response.sendStatus(404);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
