import User from '../model/user';
import Authentication from '../model/authentication';

export default function login (db, logger) {
  const userModel = new User(db, logger);
  const authenticationModel = new Authentication(db, logger);

  return async function (request, response) {
    try {
      const {
        username, password,
      } = request.body;

      const verifiedUser = await userModel.verifyLogin(username, password);
      if (!verifiedUser) {
        return response.status(401).json({ message: 'Login failed' });
      }

      const token = await authenticationModel.generate(verifiedUser);
      return response.status(200).json(token);
    } catch (error) {
      logger.error(error);
      return response.status(401).json({ message: 'Login failed' });
    }

  };
}
