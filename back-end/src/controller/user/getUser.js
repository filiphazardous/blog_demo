import User from '../../model/user';

export default function getUser (db, logger) {
  const userModel = new User(db, logger);

  return async function (request, response) {
    try {
      const {
        username,
      } = request.params;

      const { user: currentUser } = request;

      if (currentUser.username === username) {
        return response.status(200).json(currentUser);
      }

      if (currentUser.userStatus !== 'admin') {
        return response.sendStatus(403);
      }

      const user = await userModel.fetchByUsername(username);

      return response.status(200).json(user);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
