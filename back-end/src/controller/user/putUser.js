import User from '../../model/user';

export default function putUser (db, logger) {
  const userModel = new User(db, logger);

  return async function (request, response) {
    try {
      const {
        username: usernameFromParams,
      } = request.params;

      const {
        user: currentUser,
      } = request;

      if (usernameFromParams !== currentUser.username && currentUser.userStatus !== 'admin') {
        return response.sendStatus(403);
      }

      logger.log(`update user: ${usernameFromParams}`);

      const userFromDb = usernameFromParams === currentUser.username ? currentUser : await userModel.fetchByUsername(usernameFromParams);

      const {
        username = usernameFromParams,
        firstName = userFromDb.firstName,
        lastName = userFromDb.lastName,
        email = userFromDb.email,
        phone = userFromDb.phone,
        password: unhashedPassword,
      } = request.body;
      const password = unhashedPassword ? User.hashPassword(unhashedPassword) : userFromDb.password;

      const user = { username, firstName, lastName, email, password, phone };

      // Only admin gets to update userStatus
      if (request.body.userStatus) {
        if (currentUser.userStatus === 'admin') {
          user.userStatus = request.body.userStatus;
        } else {
          return response.sendStatus(403);
        }
      }

      const result = await userModel.updateByUsername(usernameFromParams, user);

      return response.status(200).json(result);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
