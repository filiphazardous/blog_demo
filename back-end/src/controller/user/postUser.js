import User from '../../model/user';

export default function postUser (db, logger) {
  const userModel = new User(db, logger);

  return async function (request, response) {
    try {
      const {
        username,
        firstName = null,
        lastName = null,
        email,
        password: unhashedPassword,
        phone = null,
      } = request.body;
      const password = User.hashPassword(unhashedPassword);

      const user = { username, firstName, lastName, email, password, phone };
      const result = await userModel.add(user);

      return response.status(201).json(result);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
