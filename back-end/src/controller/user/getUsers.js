import User from '../../model/user';

export default function getUsers (db, logger) {
  const userModel = new User(db, logger);

  return async function (request, response) {
    try {
      const {
        user: currentUser,
      } = request;

      if (currentUser.userStatus !== 'admin') {
        return response.sendStatus(403);
      }

      const {
        status,
      } = request.query;
      const statusAr = typeof status === 'string' ? status.split(',') : null;
      const users = await userModel.fetchByStatus(statusAr);
      return response.status(200).json(users);

    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
