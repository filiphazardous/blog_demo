import User from '../../model/user';

export default function deleteUser (db, logger) {
  const userModel = new User(db, logger);

  return async function (request, response) {
    try {
      const {
        username,
      } = request.params;

      logger.log(`delete user: ${username}`);

      const { user: currentUser } = request;

      if (!currentUser || currentUser.userStatus !== 'admin') {
        return response.sendStatus(403);
      }
      await userModel.deleteByUsername(username);

      return response.sendStatus(204);
    } catch (error) {
      logger.error(error);
      return response.status(500).json('Internal server error');
    }
  };
}
