import { DATABASE_STATUSES, getDatabaseStatus } from '../utils/db';

function healthController (logger = null, db = null) {
  return function (request, response) {
    const dbStatus = getDatabaseStatus();
    switch (dbStatus) {
      case DATABASE_STATUSES.UP:
        return response.status(200).json('Ok');
      case DATABASE_STATUSES.INITIALIZING:
      case DATABASE_STATUSES.READY:
        return response.status(503).json({ error: { code: 503, message: 'Database initializing' } });
      case DATABASE_STATUSES.DOWN:
      case DATABASE_STATUSES.NOT_STARTED:
        return response.status(503).json({ error: { code: 503, message: 'Database down' } });
      default:
        return response.status(503).json({ error: { code: 500, message: 'Internal server error' } });
    }
  };
}

export default healthController;
