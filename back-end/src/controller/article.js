import deleteArticle from './article/deleteArticle';
import getArticle from './article/getArticle';
import getArticles from './article/getArticles';
import postArticle from './article/postArticle';
import putArticle from './article/putArticle';

export default {
  deleteArticle,
  getArticle,
  getArticles,
  postArticle,
  putArticle,
};
