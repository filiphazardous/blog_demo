import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import { initDatabase } from './utils/db';
import healthRoutes from './route/health';
import authenticationRoutes from './route/authentication';
import userRoutes from './route/user';
import articleRoutes from './route/article';

const serverPort = process.env.SERVER_PORT || 3001;
const basePath = process.env.BASE_PATH || 'api';

console.log(`NODE_ENV: "${process.env.NODE_ENV}"`);

// Bootstrap express.js server
const apiServer = express();
apiServer.use(cors());
apiServer.use(bodyParser.urlencoded({ extended: true }));
apiServer.use(bodyParser.json());

// Initialize database
const db = initDatabase(console);

// Register routes
healthRoutes(basePath, apiServer, console, db);
authenticationRoutes(basePath, apiServer, console, db);
userRoutes(basePath, apiServer, console, db);
articleRoutes(basePath, apiServer, console, db);

// Setup listenFunc
function listenFunc (err) {
  function handleExit () {
    process.exit(0);
  }

  process.on('SIGTERM', handleExit);
  process.on('SIGINT', handleExit);
}

export default {
  apiServer,
  listenFunc,
  serverPort,
};
