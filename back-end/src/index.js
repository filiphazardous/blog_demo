import serverVars from './server';

const { apiServer, serverPort, listenFunc } = serverVars;
console.log(`Setting up server to listen on port ${serverPort}`);
apiServer.listen(serverPort, listenFunc);
