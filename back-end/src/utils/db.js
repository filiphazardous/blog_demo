import Knex from 'knex';
import sleep from './sleep';

const MYSQL_PASSWORD = process.env.MYSQL_PASSWORD;
const MYSQL_CONNECTION_STRING = process.env.MYSQL_CONNECTION_STRING || `mysql://blog_demo:${MYSQL_PASSWORD}@localhost:3306/blog_demo`;
const MYSQL_HEALTH_UPDATE_INTERVAL = process.env.MYSQL_HEALTH_UPDATE_INTERVAL || 250;

export const DATABASE_STATUSES = {
  INITIALIZING: 'INITIALIZING',
  READY: 'READY',
  UP: 'UP',
  DOWN: 'DOWN',
  INSERTING_DUMMY_DATA: 'INSERTING_DUMMY_DATA',
  NOT_STARTED: 'NOT_STARTED',
};

let status = DATABASE_STATUSES.NOT_STARTED;
let db = null;
let logger = null;

export function initDatabase (loggerInput) {
  logger = loggerInput;
  if (db !== null) {
    return db;
  }
  try {
    status = DATABASE_STATUSES.INITIALIZING;

    db = new Knex({
      client: 'mysql2',
      connection: MYSQL_CONNECTION_STRING,
      pool: {
        afterCreate (conn, done) {
          if (status === DATABASE_STATUSES.INITIALIZING) {
            status = DATABASE_STATUSES.READY;
          }
          done(null, conn);
        },
      },
    });

    // This is to init the first connection in the pool, otherwise we will not get the status update checks going
    db.raw('SELECT NOW()').then(() => logger.log('Initial database connection made'));

    updateHealthStatus();
  } catch (error) {
    logger.error('Caught error in Db constructor!');
    logger.error(error);
    return null;
  }
  return db;
}

async function updateHealthStatus () {
  if (status === DATABASE_STATUSES.INITIALIZING) {
    await sleep(MYSQL_HEALTH_UPDATE_INTERVAL);
    return updateHealthStatus();
  }
  try {
    const tableExists = await db.raw('SHOW TABLES LIKE \'user\'');
    if (tableExists) {
      status = DATABASE_STATUSES.UP;
    }
  } catch (error) {
    status = DATABASE_STATUSES.DOWN;
    logger.error(error);
  } finally {
    setTimeout(updateHealthStatus, MYSQL_HEALTH_UPDATE_INTERVAL);
  }
}

export function getDatabaseStatus () {
  return status;
}

export async function waitForDatabase () {
  if (status === DATABASE_STATUSES.UP) {
    return;
  }
  await sleep(MYSQL_HEALTH_UPDATE_INTERVAL);
  return waitForDatabase();
}
