import swaggerUi from 'swagger-ui-express';
import YAML from 'js-yaml';
import fs from 'fs';
import serverVars from './server';

const { apiServer, listenFunc, serverPort } = serverVars;

const swaggerDocument = YAML.load(fs.readFileSync(`${__dirname}/../../api-doc/back-end-api.yaml`, 'utf8'));
apiServer.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

console.log(`Setting up server to listen on port ${serverPort}`);
apiServer.listen(serverPort, listenFunc);
