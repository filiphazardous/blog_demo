import Authentication from '../model/authentication';

export default function (db, logger, authRequired) {
  const authenticationModel = new Authentication(db, logger);

  return async function (request, response, next) {
    try {
      const header = request.headers.authorization;
      const token = typeof header === 'string' ? header.split(' ')[1] : null;

      if (!token) {
        if (authRequired) {
          return response.status(401).send();
        }
        return next();
      }

      const user = await authenticationModel.authenticate(token);
      if (!user) { // Should not happen, but wth
        throw new Error('No user returned from auth');
      }

      request.user = user;
      return next();
    } catch (error) {
      logger.error(error);
      if (!authRequired) {
        request.tokenError = error.name;
        return next();
      }
      return response.status(403).send();
    }
  };
}
