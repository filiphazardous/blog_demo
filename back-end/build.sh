#!/bin/bash

rm -rf dist
mkdir dist

rm jwtSecret.json
node scripts/genJwtSecret.js > jwtSecret.json

npx babel src --out-dir dist
