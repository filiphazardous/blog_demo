--
-- Drop existing tables, if any
--

DROP TABLE IF EXISTS `article_tag`;
DROP TABLE IF EXISTS `article`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `user_status`;
DROP TABLE IF EXISTS `tag`;
DROP TABLE IF EXISTS `article_status`;
DROP TABLE IF EXISTS `category`;

--
-- Table structure for table `article_status`
--

CREATE TABLE `article_status`
(
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `article_status`
--

LOCK
TABLES `article_status` WRITE;
INSERT INTO `article_status`
VALUES ('pending'),
       ('published'),
       ('removed');
UNLOCK
TABLES;

--
-- Table structure for table `category`
--

CREATE TABLE `category`
(
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `tag`
--

CREATE TABLE `tag`
(
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `user_status`
--

CREATE TABLE `user_status`
(
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_status`
--

LOCK
TABLES `user_status` WRITE;
INSERT INTO `user_status`
VALUES ('active'),
       ('admin'),
       ('pending'),
       ('removed');
UNLOCK
TABLES;

--
-- Table structure for table `user`
--

CREATE TABLE `user`
(
    `id`         varchar(36)  NOT NULL DEFAULT (uuid()),
    `createdAt`  timestamp    NULL     DEFAULT (now()),
    `updatedAt`  timestamp    NULL     DEFAULT (now()),
    `username`   varchar(255) NOT NULL,
    `firstName`  varchar(255)          DEFAULT NULL,
    `lastName`   varchar(255)          DEFAULT NULL,
    `email`      varchar(255) NOT NULL,
    `password`   varchar(255) NOT NULL,
    `phone`      varchar(255)          DEFAULT NULL,
    `userStatus` varchar(255) NOT NULL DEFAULT (_utf8mb4'pending'),
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`),
    UNIQUE KEY `email` (`email`),
    KEY `user_status` (`userStatus`),
    CONSTRAINT `user_ibfk_1` FOREIGN KEY (`userStatus`) REFERENCES `user_status` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `article`
--

CREATE TABLE `article`
(
    `id`            varchar(36)  NOT NULL DEFAULT (uuid()),
    `author`        varchar(36)  NOT NULL,
    `createdAt`     timestamp    NOT NULL DEFAULT (now()),
    `updatedAt`     timestamp    NOT NULL DEFAULT (now()),
    `title`         varchar(255) NOT NULL,
    `text`          text         NOT NULL,
    `articleStatus` varchar(255) NOT NULL DEFAULT ('pending'),
    `category`      varchar(255)          DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `author` (`author`),
    KEY `article_status` (`articleStatus`),
    KEY `category` (`category`),
    CONSTRAINT `article_ibfk_1` FOREIGN KEY (`author`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `article_ibfk_2` FOREIGN KEY (`articleStatus`) REFERENCES `article_status` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `article_ibfk_3` FOREIGN KEY (`category`) REFERENCES `category` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag`
(
    `article` varchar(36)  DEFAULT NULL,
    `tag`     varchar(256) DEFAULT NULL,
    KEY `article` (`article`),
    KEY `tag` (`tag`),
    CONSTRAINT `article_tag_ibfk_1` FOREIGN KEY (`article`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `article_tag_ibfk_2` FOREIGN KEY (`tag`) REFERENCES `tag` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
