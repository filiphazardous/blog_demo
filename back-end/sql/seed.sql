DELETE FROM `user`;
DELETE FROM `article_tag`;
DELETE FROM `article`;
DELETE FROM `tag`;
DELETE FROM `category`;

-- Insert lines into user table
INSERT INTO `user` VALUES ('2ef9156c-780f-11eb-a185-738e728d6e81','2021-02-26 08:46:57','2021-02-26 08:46:57','test5','annat namn','efternamn','test5@test.com','$2a$10$Se0GknxFDjfYvXTj9vXJv..PUtmts.9UUFg2xLT14T25vl9eZIw8.','0123456789','pending');
INSERT INTO `user` VALUES ('770b0c2e-773e-11eb-8941-c88d675fa82b','2021-02-25 07:52:53','2021-02-25 07:52:53','test3','förnamn','efternamn','test2@test.com','$2a$10$rlZrg0zF7BY0NHNXh9.YG.Z/BobuyKejLMCqylj9wS2owkuvLtxQa','0123456789','active');
INSERT INTO `user` VALUES ('cf5d7cea-76ad-11eb-8941-c88d675fa82b','2021-02-24 14:37:24','2021-02-24 14:37:24','test','first name','last name','test@test.com','$2a$10$zi2EizId37qAmY4xnsLxfuJJ4.14H/JpmlWuHkD8m/ZpG36SQD/FW','0123456789','admin');

-- TODO: Insert lines into category table
-- TODO: Insert lines into tag table
-- TODO: Insert lines into article_tag table

-- Insert lines into article table
INSERT INTO `article` VALUES ('440b4ee6-77b7-11eb-8941-c88d675fa82b','770b0c2e-773e-11eb-8941-c88d675fa82b','2021-02-25 22:17:36','2021-02-25 22:17:36','Article 4','Lorem ipsum and so on','published',NULL);
INSERT INTO `article` VALUES ('9fd52208-780f-11eb-a185-738e728d6e81','770b0c2e-773e-11eb-8941-c88d675fa82b','2021-02-26 08:50:06','2021-02-26 08:50:06','Article 5','Lorem ipsum and so on','pending',NULL);
INSERT INTO `article` VALUES ('c2d24e9c-77b6-11eb-8941-c88d675fa82b','770b0c2e-773e-11eb-8941-c88d675fa82b','2021-02-25 22:14:00','2021-02-25 22:14:00','Article 2','Lorem ipsum and so on and on','published',NULL);
INSERT INTO `article` VALUES ('e690658a-77b6-11eb-8941-c88d675fa82b','770b0c2e-773e-11eb-8941-c88d675fa82b','2021-02-25 22:15:00','2021-02-25 22:15:00','Article 3','Lorem ipsum and so on','pending',NULL);
