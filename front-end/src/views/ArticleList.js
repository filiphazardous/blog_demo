import { useEffect, useState } from 'react';
import Article from '../components/Article';

export default function ArticleList ({ token }) {
  const [articleList, setArticleList] = useState([]);

  useEffect(function () {
    try {
      const authHeader = token ? { Authorization: `Bearer ${token}` } : {};
      fetch('/api/articles', {
        headers: {
          ...authHeader,
          Accept: 'application/json',
        },
      }).then(response => response.json())
        .then(list => setArticleList(list))
        .catch(error => console.error(error));
    } catch (error) {
      console.error(error);
    }
  }, [token, setArticleList]);

  return <main>
    <h1>Articles</h1>
    {articleList.map(a => <Article key={a.id} {...a}/>)}
  </main>;
}
