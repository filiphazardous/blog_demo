import { useState } from 'react';

export default function Login ({ setLogin }) {
  const [error, setError] = useState(null);

  async function onSubmit (e) {
    e.preventDefault();
    const username = e.target['username'].value;
    const password = e.target['password'].value;

    if (!username || !password) {
      setError('Both username and password are needed to login!');
      return;
    }

    try {
      const response = await fetch('/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username,
          password,
        }),
      }).then(r => r.json());

      if (typeof response === 'object' && response.message) {
        setError(response.message);
        return;
      }

      e.target.reset();

      // The token contains its time of expire, let's extract that
      const tokenData = response.split('.')[1];
      const decodedTokenData = atob(tokenData);
      const { exp: expiresAt, userStatus } = JSON.parse(decodedTokenData);

      setLogin({ username, userStatus, token: response, expiresAt });
    } catch (error) {
      setError(error.message);
    }
  }

  return <form onSubmit={onSubmit}>
    <label htmlFor='login-username'>Username:</label>
    <input type='text' name='username' id='login-username'/>
    <label htmlFor='login-password'>Password:</label>
    <input type='password' name='password' id='login-password'/>
    <button type='submit'>Login</button>
    {error && <span>{error}</span>}
  </form>;
}
