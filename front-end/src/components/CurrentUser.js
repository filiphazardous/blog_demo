const statusColors = {
  admin: 'blue',
  active: 'green',
  pending: 'orange',
};

export default function CurrentUser ({ username, userStatus }) {
  return <p style={{ color: statusColors[userStatus] }}>User: {username}, Status: {userStatus}</p>;
}
