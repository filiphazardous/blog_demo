import { useEffect, useState } from 'react';
import Login from './components/Login';
import CurrentUser from './components/CurrentUser';
import ArticleList from './views/ArticleList';

function App () {
  const [{ token, username, userStatus, expiresAt }, setLogin] = useState({});

  // Clear token when it expires
  useEffect(function () {
    if (expiresAt) {
      const timeDelta = expiresAt * 1000 - Date.now();
      console.log({ timeDelta });
      setTimeout(function () {
        console.log('Token expired');
        setLogin({});
      }, timeDelta);
    }
  }, [expiresAt, setLogin]);

  return (
    <div className="App">
      <header className="App-header">
        {!token && (<Login setLogin={setLogin}/>)}
        {username && (<CurrentUser username={username} userStatus={userStatus}/>)}
      </header>
      <ArticleList token={token}/>
    </div>
  );
}

export default App;
